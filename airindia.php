<?php
require 'functions.php';
require './src/facebook.php';
$facebook = new Facebook(array(
            'appId' => $config["fb_app_id"],
            'secret' => $config["fb_app_secret"],
            'cookie' => true,
        ));

$uid = $facebook->getUser();
$wrong = FALSE;
$class = "form-group";
$lvl = 14;
require 'checkans.php';
$user_profile = null;
// Session based API call.
if ($uid) {
    try {
        // We can use a Graph API call from the PHP-SDK
        // Or just use our Database! get_user_by_id($uid)
        $user_profile = $facebook->api('/me');
    } catch (FacebookApiException $e) {
        error_log($e);
        $uid = null;
    }
}
else{
	header("location: index.php");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Air India</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="./css/bootstrap.css" media="screen">
	<link rel="stylesheet" type="text/css" href="./css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="./css/style.css" media="screen">
</head>
<body>
	<div id="fb-root"></div>
	<script>
	  window.fbAsyncInit = function() {
	    // init the FB JS SDK
	    FB.init({
	      appId      : '<?php echo $config["fb_app_id"]; ?>',                        // App ID from the app dashboard
	      channelUrl : '//<?php echo $config["base_url"]; ?>/channel.html', // Channel file for x-domain comms
	      cookie     : true,
	      status     : true,                                 // Check Facebook Login status
	      xfbml      : true                                  // Look for social plugins on the page
	    });

	    // Additional initialization code such as adding Event Listeners goes here
      FB.Event.subscribe('auth.logout', function () {
          window.location = "./index.php";
      });	    
	  };

	  // Load the SDK asynchronously
	  (function(d, s, id){
	     var js, fjs = d.getElementsByTagName(s)[0];
	     if (d.getElementById(id)) {return;}
	     js = d.createElement(s); js.id = id;
	     js.src = "//connect.facebook.net/en_US/all.js";
	     fjs.parentNode.insertBefore(js, fjs);
	   }(document, 'script', 'facebook-jssdk'));
	</script>
	<!--header-->
	<?php include 'menu.php'; ?>

	<!-- Page contents -->
	<div class="container">
		<div class="page-header">
			<h1>LEVEL <?php echo $lvl; ?> <small></small>
			<?php if($uid) { ?>
			<a href="https://www.facebook.com/<?php echo $uid; ?>/" target="_blank"><img class="pull-right" src="https://graph.facebook.com/<?php echo $uid; ?>/picture" alt="profile_pic"></a>
			<?php } ?></h1>
		</div>
		<div class="row">
			<div class="col-md-3">
				<a href="./img/nextnumber.png" target="_blank" class="thumbnail">
					<img src="./img/nextnumber.png" class="img-responsive" title="">
				</a>
			</div>
		</div>
		<br/>
	</div>
	<div class="modal fade" id="leader" tabindex="-1" role="dialog" aria-labelledby="leaderlabel" aria-hidden="true">
	    <div class="modal-dialog">
	      <div class="modal-content">
	        <div class="modal-header">
	          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	          <h4 class="modal-title">Leaderboard</h4>
	        </div>
	        <div class="modal-body">
	        	<?php
	          include 'leaderboard.php'; ?>
	        </div>
	        <div class="modal-footer">
	          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        </div>
	      </div><!-- /.modal-content -->
	    </div><!-- /.modal-dialog -->
	  </div><!-- /.modal -->
	<div class="modal fade" id="hints" tabindex="-1" role="dialog" aria-labelledby="hintslabel" aria-hidden="true">
	    <div class="modal-dialog">
	      <div class="modal-content">
	        <div class="modal-header">
	          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	          <h3 class="modal-title">HINTS</h3>
	        </div>
	        <div class="modal-body">
	        	<p class="lead">TRY IT FIRST!</p>
	        </div>
	        <div class="modal-footer">
	          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        </div>
	      </div><!-- /.modal-content -->
	    </div><!-- /.modal-dialog -->
	  </div><!-- /.modal -->
	<div class="modal fade" id="rules" tabindex="-1" role="dialog" aria-labelledby="ruleslabel" aria-hidden="true">
	    <div class="modal-dialog">
	      <div class="modal-content">
	        <div class="modal-header">
	          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	          <h4 class="modal-title">Rules</h4>
	        </div>
	        <div class="modal-body">
	        	<p>1) The aim of this event is getting ahead to the next level in any way possible.<br>
	        		2) Use URL manipulation, image manipulation, viewing page source and any method you can think of!<br>
	        		3) If needed, hints will be posted by admin on facebook page.<br>
	        		4) Posting answers or direct hints on discussion page shall invite disqualification.<br>
	        		5) Answers will be in lowercase letters, no punctuation marks, no spaces.<br>
	        		6) If the answer contains a number write it in words.<br>
	        		(eg. K9 can be written as "knine" and 2012 as "twozeroonetwo".)</p>
	        </div>
	        <div class="modal-footer">
	          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        </div>
	      </div><!-- /.modal-content -->
	    </div><!-- /.modal-dialog -->
	  </div><!-- /.modal -->
	<!-- Footer -->
<?php include 'footer.html'; ?>
</body>
</html>