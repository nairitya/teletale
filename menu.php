	<nav class="navbar navbar-default navbar-static-top" role="navigation">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
				<span class="sr-only">Toggle Navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="./home.php">DEQUODE</a>
		</div>
		<div class="collapse navbar-collapse navbar-ex1-collapse">
			<ul class="nav navbar-nav">
				<li><a data-toggle="modal" href="#leader">Leaderboard</a></li>
				<li><a data-toggle="modal" href="#hints">HINTS!</a></li>
				<li><a data-toggle="modal" href="#rules">Rules</a></li>
				<li><a href="http://www.facebook.com/thomso.dequode" target="_blank">Like Us on Facebook!</a></li>
				<li><a href="#">DEQUODE has ENDED!</a></li>
			</ul>
			<form class="navbar-form navbar-right">
				<div class="fb-login-button" size="xlarge" autologoutlink="true" registration-url="<?php echo $config["base_url"]; ?>/register.php"></div>
			</form>
		</div>
	</nav><!--/.navbar -->