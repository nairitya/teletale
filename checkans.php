<?php

require_once("db.php");

if(isset($_POST)){

	if(isset($_POST['ans'])){
		require_once('securimage/securimage.php');
		$securimage = new Securimage();
		if ($securimage->check($_POST['captcha_code']) == false) 
		{
			echo "The security code entered was incorrect.<br /><br />";
			echo "Please go <a href='/level/lvl0.php'>back</a> and try again.";
			exit;
		}
		/* ends captcha */
	}

	$level = get_current_level($uid)[0];
    if($level != $lvl){
    	header("location: lvl".$level.".php");
    }

	if($level>=$lvl){

		if(isset($_POST['ans'])){

			$ans = sha1($_POST['ans']);
			$ans = md5($ans);

			$answer = get_lvl_ans($lvl,$db);
			if($ans==$answer){
				if($level==$lvl){
					$level++;
					update_user($uid, $level, $lvl);
					$current_score = get_user_score($uid);
					$score = get_question_score($lvl);
					set_user_score($uid, $current_score[0]+$score[0]);				
				}
				
				header("Location: lvl".($lvl+1).".php");
			}
			else{
				$wrong = TRUE;
				$class = "form-group has-error";
			}
		}

		if(isset($_POST['skip'])){
			
			$level++;
			update_user($uid, $level, $lvl);
			
			header("Location: lvl".($lvl+1).".php");
		}
	}
	else{
		header("Location: lvl".($level).".php");
	}
}
?>