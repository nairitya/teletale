<?php

define("HOST", "localhost");
define("DB", "dequode");
define("USER", "root");
define("PASS", "toor");
$mysqli = new mysqli(HOST, USER, PASS, DB);

function add_user($email, $cogni_id, $nick){
	global $mysqli;
	$query = "SELECT id FROM users WHERE cogni_id = '" .$cogni_id."'";
	$result = $mysqli->query($query);
	if($result->fetch_row()) {
		return "Already Signed up !!";
	}

	$query = "SELECT id FROM users WHERE nick = '" .$nick."'";
	$result = $mysqli->query($query);
	if($result->fetch_row()) {
		return "Nick exists";
	}

	else {
		$query = "INSERT into users (cogni_id, email, nick) VALUES ('".$cogni_id ."', '" . $email. "', '" . $nick ."')";
		// echo $query;
		$result = $mysqli->query($query);
		// echo $result;
		return "true";
	}
}

function isuser_registered($cogni_id){
	global $mysqli;
	$query = "SELECT id FROM users WHERE cogni_id = '" .$cogni_id."'";
	$result = $mysqli->query($query);
	if($result->fetch_row()) {
		return true;
	} else {
		return false;
	}
}

function get_user_email($cogni_id){
	global $mysqli;
	$query = "SELECT email FROM users WHERE cogni_id = '" .$cogni_id."'";
	$result = $mysqli->query($query);
	if($result) {
		return $result->fetch_row();
	} else {
		return false;
	}
}

function get_user_score($uid){
	global $mysqli;
	$query = "SELECT current_score FROM users WHERE cogni_id = '".$uid."'";

	$result = $mysqli->query($query);
	if($result){
		return $result->fetch_row();
	} else {
		return null;
	}
}

function get_question_score($lvl){
	global $mysqli;
	$query = "SELECT score FROM score WHERE level = ".$lvl;
	$result = $mysqli->query($query);
	if($result){
		return $result->fetch_row();
	} else {
		return null;
	}
}

function set_user_score($uid, $score){
	global $mysqli;
	$query = "UPDATE users SET current_score = ".$score." WHERE cogni_id = '".$uid."'";
	$result = $mysqli->query($query);
}

function get_current_level($uid){
	global $mysqli;
	$query = "SELECT level FROM users WHERE cogni_id = '".$uid."'";
	$result = $mysqli->query($query);
	if($result){
		return $result->fetch_row();
	} else {
		return -1;
	}
}

function update_user($cogni_id, $level, $lvl){
	global $mysqli;
	$date = date('Y-m-d G:i:s');
	$query = "UPDATE users SET level = ".$level.", lvl".$lvl."= '".$date."', timest = '".$date."' WHERE cogni_id = '".$cogni_id."'";
	$result = $mysqli->query($query);
	if($result){
		return 1;
	} else {
		return -1;
	}
}

function get_lvl_ans($lvl){
	global $mysqli;
	$query = "SELECT ans FROM answers WHERE lvl = ".$lvl;
	$result = $mysqli->query($query);
	if($result){
		$ans = $result->fetch_row();
		return $ans[0];
	} else {
		return -1;
	}
}

function leaderboard_helper(){
	global $mysqli;
	$query = "SELECT nick, email, current_score FROM users order by current_score desc, timest asc";
	$result = $mysqli->query($query);
	return $result;
}

function get_user_lvl($cogni_id){
	global $mysqli;
	$query = "SELECT level FROM users WHERE cogni_id = '".$cogni_id."'";
	$result = $mysqli->query($query);
	if($result){
		$ans = $result->fetch_row();
		return $ans[0];
	} else {
		return -1;
	}
}

function update_user_nick($cogni_id, $nick){
	global $mysqli;
	$query = "UPDATE users SET nick = '".$nick."' WHERE cogni_id = '".$cogni_id."'";
	$result = $mysqli->query($query);
	if($result){
		return true;
	} else {
		return -1;
	}
}

function not_set_nick($cogni_id){
	global $mysqli;
	$query = "SELECT nick FROM users WHERE cogni_id = '".$cogni_id."'";
	$result = $mysqli->query($query);
	if($result){
		$row = $result->fetch_row();
		if($row[0] == $cogni_id){
			return true;
		} else {
			return false;
		}
	} else {
		false;
	}
}
?>