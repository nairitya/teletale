<?php
session_start();

if($_SESSION['NICK_SET'] == "NO"){

} else {
	header("Location: /index.php");
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Teletale - Rebooted!</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="./css/bootstrap.css" media="screen">
	<link rel="stylesheet" type="text/css" href="./css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="./css/style.css" media="screen">
	
	<link rel='stylesheet' href='http://codepen.io/assets/libs/fullpage/jquery-ui.css'>

    <link rel="stylesheet" href="css/style1.css" media="screen" type="text/css" />
	<script type="text/javascript" src="js/jquery.js"></script>
	
	
</head>
<body>
	
	<!--header-->
	<nav class="navbar navbar-default navbar-static-top" role="navigation">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
				<span class="sr-only">Toggle Navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="./index.php">TELETALE</a>
		</div>
		<div class="collapse navbar-collapse navbar-ex1-collapse">
			<ul class="nav navbar-nav">
				<li><a data-toggle="modal" href="#leader">Leaderboard</a></li>
				<li><a data-toggle="modal" href="#rules">Rules</a></li>
			</ul>
			
		</div>
	</nav><!--/.navbar -->

	<!-- Page contents -->
	<div class="container">
		<div class="page-header">
			<h1>Teletale <small>The Mystery is back!</small>
			</h1>
			<?php if(isset($_SESSION['ERROR'])){
				echo '<h1 style="margin-top: -50px;margin-left: 950px;">'.$_SESSION['ERROR'].'</h1>';
				unset($_SESSION['ERROR']);
			}
			?>
		</div>
		<div class="col-md-12">
		
       	<p class="lead"></p>
       	<!-- <a class="btn btn-default btn-lg" href="./home.php" style="<?php echo $show; ?>">Go Play</a> -->
       	</div>
	</div>
	<div class="modal fade" id="leader" tabindex="-1" role="dialog" aria-labelledby="leaderlabel" aria-hidden="true">
	    <div class="modal-dialog">
	      <div class="modal-content">
	        <div class="modal-header">
	          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	          <h4 class="modal-title">Leaderboard</h4>
	        </div>
	        <div class="modal-body">
	          
	        </div>
	        <div class="modal-footer">
	          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        </div>
	      </div><!-- /.modal-content -->
	    </div><!-- /.modal-dialog -->
	  </div><!-- /.modal -->
	<div class="modal fade" id="rules" tabindex="-1" role="dialog" aria-labelledby="ruleslabel" aria-hidden="true">
	    <div class="modal-dialog">
	      <div class="modal-content">
	        <div class="modal-header">
	          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	          <h4 class="modal-title">Rules</h4>
	        </div>
	        <div class="modal-body">
	        	<p>1) The aim of this event is getting ahead to the next level in any way possible.<br>
	        		2) Use URL manipulation, image manipulation, viewing page source and any method you can think of!<br>
	        		3) If needed, hints will be posted by admin on facebook page.<br>
	        		4) Posting answers or direct hints on discussion page shall invite disqualification.<br>
	        		5) Answers will be in lowercase letters, no punctuation marks, no spaces.<br>
	        		6) If the answer contains a number write it in words.<br>
	        		(eg. K9 can be written as "knine" and 2012 as "twozeroonetwo".)</p>
	        </div>
	        <div class="modal-footer">
	          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        </div>
	      </div><!-- /.modal-content -->
	    </div><!-- /.modal-dialog -->
	  </div><!-- /.modal -->
	<!-- Footer -->

	<div class="login-card">
    <h1 id="f1" style="cursor:pointer;font-size:22px;color:#3079ed;">Enter your nick here</h1>
    <br>
  <form id="login" action="auth.php" method="post">
    <input type="text" name="nick" placeholder="Nickname">
    <input type="submit" name="login" class="login login-submit" value="Proceed">
  </form>

  
</div>

</body>
</html>