<?php
session_start();

if (isset($_SESSION['CID'])) {
    //Good
}
else {
	header("location: index.php");
}

if($_SESSION['NICK_SET'] == "NO"){
	header("Location: /nick.php");
} else {

}

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Teletale - Rebooted!</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="./css/bootstrap.css" media="screen">
	<link rel="stylesheet" type="text/css" href="./css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="./css/style.css" media="screen">
</head>
<body>
	<div id="fb-root"></div>
	<nav class="navbar navbar-default navbar-static-top" role="navigation">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
				<span class="sr-only">Toggle Navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="./index.php">Teletale</a>
		</div>
		<div class="collapse navbar-collapse navbar-ex1-collapse">
			<ul class="nav navbar-nav">
				<li><a data-toggle="modal" href="#leader">Leaderboard</a></li>
				<li><a data-toggle="modal" href="#rules">Rules</a></li>
			</ul>
			<form class="navbar-form navbar-right">
				<div style="margin-top: 6px;"><a href="/logout.php" style="color: #777777;">Logout</a></div>
			</form>
		</div>
	</nav><!--/.navbar -->

	<!-- Page contents -->
	<div class="container">
		<div class="page-header">
			<h1>Teletale <small>The Mystery is back!</small></h1>
		</div>
		<div class="row">
			<?php
			require_once("db.php");
			$lvl = get_user_lvl($_SESSION['CID']);
			?>
			<a class="btn btn-default btn-lg" href="./level/lvl<?php echo $lvl; ?>.php">Level <?php echo $lvl; ?></a>
			
		</div>
	</div>
	<div class="modal fade" id="leader" tabindex="-1" role="dialog" aria-labelledby="leaderlabel" aria-hidden="true">
	    <div class="modal-dialog">
	      <div class="modal-content">
	        <div class="modal-header">
	          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	          <h4 class="modal-title">Leaderboard</h4>
	        </div>
	        <div class="modal-body">
	        	<?php
	          include 'leaderboard.php'; ?>
	        </div>
	        <div class="modal-footer">
	          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        </div>
	      </div><!-- /.modal-content -->
	    </div><!-- /.modal-dialog -->
	  </div><!-- /.modal -->
	<div class="modal fade" id="rules" tabindex="-1" role="dialog" aria-labelledby="ruleslabel" aria-hidden="true">
	    <div class="modal-dialog">
	      <div class="modal-content">
	        <div class="modal-header">
	          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	          <h4 class="modal-title">Rules</h4>
	        </div>
	        <div class="modal-body">
	        	<p>1 This is an individual contest of 2 hrs duration.<br>
				2 The questions are divided in 64 levels, each level containing a single question with the corresponding marks displayed on the top right.<br>
3 You can only move forward in levels that is if you skip any question and move forward in levels, you can’t come back to previous levels.<br>
 4 Use only lower case letters or digits to answer the questions. For example if answer is ‘Ross’ you should submit ‘ross’ .If the answer is ‘10’ report it as ‘10’ and not ‘ten’. <br>
5.The exception of above rule is in level 60 where you have to submit answer in uppercase letters. <br>
6.Any notifications or updates will appear on the home page.<br>
7.In case of a tie in total score, the winner will be the one who has solved these questions first. <br>
8. It is not necessary that the scores corresponding to question will increase with level.<br>
	        </div>
	        <div class="modal-footer">
	          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        </div>
	      </div><!-- /.modal-content -->
	    </div><!-- /.modal-dialog -->
	  </div><!-- /.modal -->
	<div class="modal fade" id="hints" tabindex="-1" role="dialog" aria-labelledby="hintslabel" aria-hidden="true">
	    <div class="modal-dialog">
	      <div class="modal-content">
	        <div class="modal-header">
	          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	          
	        </div>
	        <div class="modal-footer">
	          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        </div>
	      </div><!-- /.modal-content -->
	    </div><!-- /.modal-dialog -->
	  </div><!-- /.modal -->
	<!-- Footer -->
<?php include 'footer.html'; ?>
</body>
</html>