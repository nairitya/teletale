<table class="table table-striped table-condensed">
  <thead>
    <tr>
      <th>Rank</th>
      <th>Nik</th>
      <th>Email</th>
      <th>Score</th>
    </tr>
  </thead>
  <tbody>
<?php 
require_once("db.php");
$counter = 0;
try {
      $stmt = leaderboard_helper();
          while ($row = $stmt->fetch_row()) {
            ++$counter;
            echo "<tr>";
            echo "<td>".$counter."</td>";
            echo "<td>".$row[0]."</td>";
            echo "<td>".$row[1]."</td>";
            echo "<td>".$row[2]."</td>";
            echo "</tr>";
          }
      }
      catch(PDOException $e) {
          echo $e->getMessage();
      }
       ?>
  </tbody>
</table>    