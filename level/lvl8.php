<?php
session_start();

$wrong = FALSE;
$class = "form-group";
$lvl = 8;


$user_profile = null;

if (isset($_SESSION['CID'])) {
    $uid = $_SESSION['CID'];
}
else {
	header("location: index.php");
}

if($_SESSION['NICK_SET'] == "NO"){
	header("Location: /nick.php");
} else {
	
}

require '../checkans.php';

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Teletale - Rebooted!</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.css" media="screen">
	<link rel="stylesheet" type="text/css" href="../css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="../css/style.css" media="screen">
</head>
<body style="overflow-x: hidden;">
	<div id="fb-root"></div>
	<nav class="navbar navbar-default navbar-static-top" role="navigation">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
				<span class="sr-only">Toggle Navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="../home.php">Teletale</a>
		</div>
		<div class="collapse navbar-collapse navbar-ex1-collapse">
			<ul class="nav navbar-nav">
				<li><a data-toggle="modal" href="#leader">Leaderboard</a></li
			</ul>
			<form class="navbar-form navbar-right">
				<div style="margin-top: 6px;margin-left: 1000px;"><a href="/logout.php" style="color: #777777;">Logout</a></div>
			</form>
		</div>
	</nav><!--/.navbar -->

	<!-- Page contents -->
	<div class="container">
		<div class="page-header">
			<h1>LEVEL <?php echo $lvl; ?> <small>(Points <?php echo get_question_score($lvl)[0];?>)</small></h1>
			<h3 style="margin-left:895px;margin-top:-35px;">Current Score :- <?php echo get_user_score($uid)[0];?></h3>
		</div>
		<img src="/img/8.png" style="width:325px;height:400px;">
		<div class="col-md-12" style="margin-left:360px;margin-top:-400px;">
			<p class="lead" style="width:830px;">Find this five letter word

			</p>
			<img src="" class="img-responsive">
			<div class="col-md-9">
				<form class="form-horizontal" action="" role="form" method="post">
					<div class="<?php echo $class; ?>">
						<div class="input-group">
							<input type="text" id="ansbox" class="form-control" name="ans" placeholder="<?php if($wrong) echo 'Wrong Answer!!!'; else echo "Answer";?>" required>
							<div style="margin-top:20px;">
					            <img style="width:130px;" id="captcha" src="../securimage/securimage_show.php" alt="CAPTCHA Image" />
					            <input type="text" name="captcha_code" size="10" maxlength="6" id="captcha_input"/>
					            <a href="#" id="change_captcha" onclick="document.getElementById('captcha').src = '../securimage/securimage_show.php?' + Math.random(); return false">[ Different Image ]</a>
					        </div>
							<span class="input-group-btn">
								<button class="btn btn-default" style="margin-top:-70px;" type="submit">Go!</button>
							</span>
						</div>
					</div>				
				</form>
				<form method="post" style="position: relative;right: 15px;" action="" role="form" class="form-horizontal">
					<input type="hidden" value="true" name="skip">
					<input type="submit" value="Skip">
				</form>


			</div>	
		</div>
	</div>
	<div class="modal fade" id="leader" tabindex="-1" role="dialog" aria-labelledby="leaderlabel" aria-hidden="true">
	    <div class="modal-dialog">
	      <div class="modal-content">
	        <div class="modal-header">
	          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	          <h4 class="modal-title">Leaderboard</h4>
	        </div>
	        <div class="modal-body">
	        	<?php include '../leaderboard.php'; ?>
	        </div>
	        <div class="modal-footer">
	          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        </div>
	      </div><!-- /.modal-content -->
	    </div><!-- /.modal-dialog -->
	  </div><!-- /.modal -->
	<div class="modal fade" id="hints" tabindex="-1" role="dialog" aria-labelledby="hintslabel" aria-hidden="true">
	    <div class="modal-dialog">
	      <div class="modal-content">
	        <div class="modal-header">
	          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        </div>
	        <div class="modal-footer">
	          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        </div>
	      </div><!-- /.modal-content -->
	    </div><!-- /.modal-dialog -->
	  </div><!-- /.modal -->
	<div class="modal fade" id="rules" tabindex="-1" role="dialog" aria-labelledby="ruleslabel" aria-hidden="true">
	    <div class="modal-dialog">
	      <div class="modal-content">
	        <div class="modal-header">
	          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	          
	        </div>
	        <div class="modal-footer">
	          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        </div>
	      </div><!-- /.modal-content -->
	    </div><!-- /.modal-dialog -->
	  </div><!-- /.modal -->
	<!-- Footer -->
	<?php include '../footer.html'; ?>
</body>
</html>
